# Markdown Master Cheatsheet
project description

## Highlighting text

Markdown itself doesn't support colored or highlighted text. However, we can use **HTML** within our Markdown, and **CSS** for styling. 

<span style="background-color: gold;">This is some text a gold background!</span>

## Code

### Inline code
This is a simple inline `code`.

### Code blocks

```html
<meta charset="UTF-8">
```

```js
let myVariable = 10;
// JetBrains CE IDE-s doesn't provide syntax highlight for every language...
```

### Project shell command
it can be run from the IDE with click

```shell
npm run my-command
```

## Tasks

- [X] Completed task
- [ ] Incomplete task
    - [x] Completed subtask
    - [ ] sub task


- [x] complete task
    - [ ] incomplete subtask1
        - [ ] incomplete subtask2

## Line 

***

## Lists
### Ordered list
1. first item
    1. sub item1
    2. sub item2
2. second item
3. etc...

### Unordered list
- item1
- item2
- etc...

## GitLab Flavored Markdown (GFMD)
https://docs.gitlab.com/ee/user/markdown.html
